# Travel the world
**Travel the world** est un projet d'intégration d'une page d'accueil d'une agence de voyage.
On peut y trouver un formulaire de recherche et de réservation, les bons plans, des endroits à visiter, un formulaire de newsletter et un formulaire de contact.
Ce projet est toujours en cours de réalisation.
## Technologies utilisées
![HTML CSS JS](http://p92.com/binaries/content/gallery/p92website/technologies/htmlcssjs-overview.png)
## Démo
[Lien vers la démo](https://brightmarc90.gitlab.io/travel-the-world/)
## Auteur
> par Marc AKPOTO-K